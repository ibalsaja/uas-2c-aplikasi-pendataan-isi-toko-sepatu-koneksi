<?php
	$DB_NAME = "uas";
	$DB_USER = "root";
	$DB_PASS = "";
	$DB_SERVER_LOC = "localhost";

	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$conn = mysqli_connect($DB_SERVER_LOC,$DB_USER,$DB_PASS,$DB_NAME);
		$sql = "select jenis from jenis order by jenis asc";
		$result = mysqli_query($conn,$sql);
		$x = mysqli_num_rows($result);
		if($x>0){
			header("Access-Control-Allow-Origin: *");
			header("Content-type: application/json; charset=UTF-8");
			$jenis = array();
			while($jenis = mysqli_fetch_assoc($result)){
				array_push($jenis, $jenis);
			}
			echo json_encode($jenis);
		}
	}
?>
