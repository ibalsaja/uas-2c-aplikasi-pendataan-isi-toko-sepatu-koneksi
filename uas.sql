/*
SQLyog Ultimate
MySQL - 10.4.11-MariaDB : Database - uas
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`uas` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `uas`;

/*Table structure for table `jenis` */

DROP TABLE IF EXISTS `jenis`;

CREATE TABLE `jenis` (
  `id_jenis` int(2) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `jenis` */

insert  into `jenis`(`id_jenis`,`jenis`) values 
(1,'high'),
(2,'low'),
(3,'slip on');

/*Table structure for table `sepatu` */

DROP TABLE IF EXISTS `sepatu`;

CREATE TABLE `sepatu` (
  `merk` varchar(10) NOT NULL,
  `warna` varchar(10) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  `ukuran` varchar(2) NOT NULL,
  `photos` varchar(10) NOT NULL,
  PRIMARY KEY (`merk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `sepatu` */

insert  into `sepatu`(`merk`,`warna`,`jenis`,`ukuran`,`photos`) values 
('','','','','2.jpg'),
('checkerboa','black','low','34','1.jpg'),
('ori slime','black','low','36','3.jpg'),
('slip check','white','slip on','37','4.jpg'),
('trasher','black','high','41','5.jpg');

/*Table structure for table `ukuran` */

DROP TABLE IF EXISTS `ukuran`;

CREATE TABLE `ukuran` (
  `id_ukuran` int(2) NOT NULL,
  `ukuran` varchar(4) NOT NULL,
  PRIMARY KEY (`id_ukuran`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `ukuran` */

insert  into `ukuran`(`id_ukuran`,`ukuran`) values 
(1,'35'),
(2,'36'),
(3,'37'),
(4,'38'),
(5,'39'),
(6,'40'),
(7,'41'),
(8,'42'),
(9,'43'),
(10,'44'),
(11,'45'),
(12,'46');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
